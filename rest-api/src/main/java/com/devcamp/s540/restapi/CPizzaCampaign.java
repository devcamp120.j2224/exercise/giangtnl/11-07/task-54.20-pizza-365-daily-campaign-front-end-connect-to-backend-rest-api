package com.devcamp.s540.restapi;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
public class CPizzaCampaign {
	@CrossOrigin
	@GetMapping("/devcamp-simple")
	public String simple() {
		return "test campaign";
	}
}

